# poll-app

Sample aplication for voting on polls

## Requirements

- Docker

## Running

### Production

#### Using docker-compose

Using docker-compose you can simply run `docker-compose up` and visit http://localhost

#### Using vanilla Docker

This project expects that you are using Docker to run your production services.

You can build image by running `docker build . -t poll-app`, and then you can run container via `docker run -p 80:80 -t poll-app` and visit http://localhost within your browser.

### Development

Simply run `yarn dev` and project will be automatically open in your browser on the first available port.

## Configuration

This project supports **runtime configuration** which means that you can change environment variable (within docker-compose or via docker flags) and you will be able to reconfigure API_URL without rebuilding images.
