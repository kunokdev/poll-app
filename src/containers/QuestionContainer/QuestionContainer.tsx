import React from "react";
import { connect } from "react-redux";
import moment from "moment";

import { reading, submission } from "redux/modules/questions";
import { StoreType } from "redux/types";
import QuestionFormContainer from "containers/QuestionFormContainer";
import objectifyArray from "utils/objectifyArray";
import vote from "instances/vote";

type PropTypes = {
  id: string;
  fetch: Function;
  selectPayload: Function;
  submit: (url: string, body?: any, params?: {}) => any;
};

type StateTypes = {
  voted: string | null;
};

class QuestionContainer extends React.Component<PropTypes, StateTypes> {
  state = {
    voted: null
  };

  async componentDidMount() {
    await this.props.fetch(this.props.id);
    this.handleVote();
  }

  handleSubmit = async ({ answer }: { answer: string }) => {
    const [choicesHashTable] = objectifyArray(
      this.props.selectPayload().choices,
      ({ url }: { url: string }) => url.split("/")[4]
    );
    const url = choicesHashTable[answer] && choicesHashTable[answer].url;
    if (!url) return;
    await this.props.submit(url);
    vote.setVote(this.props.id, url.split("/")[4]);
    await this.props.fetch(this.props.id);
    this.handleVote();
  };

  handleVote() {
    this.setState({
      voted: vote.getVote(this.props.id)
    });
  }

  render() {
    const result = this.props.selectPayload();
    if (result)
      return (
        <div>
          <h1>{result.question}</h1>
          <i>Published at {moment(result.published_at).format("MMM DD, YYYY")}</i>
          <hr />
          <QuestionFormContainer
            onSubmit={this.handleSubmit}
            answers={result.choices}
            voted={this.state.voted}
          />
        </div>
      );
    return <div>Loading...</div>;
  }
}

export default connect(
  (state: StoreType) => ({
    selectPayload: reading.selectPayload(state)
  }),
  {
    fetch: reading.fetchAnd,
    submit: submission.submitAnd
  }
)(QuestionContainer);
