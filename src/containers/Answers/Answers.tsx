import React from "react";

import Answer from "components/Answer";
import calculatePercentOutOfTotal from "utils/calculatePercent";

interface IChoice {
  url: string;
  choice: string;
  votes: number;
}

interface PropTypes {
  selected: string | null;
  voted: string | null;
  answers: IChoice[];
  onClick: Function;
}

export default class Answers extends React.Component<PropTypes> {
  render() {
    const totalVotes = (answers => {
      let buffer = 0;
      answers.forEach(({ votes }) => votes && (buffer += votes));
      return buffer;
    })(this.props.answers);

    return (
      <div>
        {this.props.answers &&
          this.props.answers.map((answer, i) => {
            const id = answer.url.split("/")[4];
            return (
              <Answer
                id={id}
                selected={this.props.selected === id}
                voted={this.props.voted}
                onClick={() => this.props.onClick({ ...answer, id }, i)}
                key={i}
                percent={calculatePercentOutOfTotal(answer.votes, totalVotes)}
                {...answer}
              />
            );
          })}
      </div>
    );
  }
}
