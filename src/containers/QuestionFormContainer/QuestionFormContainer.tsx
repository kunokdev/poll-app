import React from "react";
import { connect } from "react-redux";
import { reduxForm, FormSubmitHandler } from "redux-form";

import { QUESTIONS } from "redux/modules/questions";

import Form from "components/QuestionForm";
import { StoreType } from "redux/types";

interface PropTypes {
  onSubmit: FormSubmitHandler<any>;
  answers: [];
  voted: string | null;
}

class QuestionFormContainer extends React.PureComponent<PropTypes> {
  Form = reduxForm({
    form: QUESTIONS,
    onSubmit: this.props.onSubmit,
    enableReinitialize: true
  })(Form);

  render() {
    return <this.Form {...this.props} />;
  }
}

export default connect(
  (state: StoreType) => ({
    selectedAnswer:
      state.form &&
      state.form[QUESTIONS] &&
      state.form[QUESTIONS].values &&
      state.form[QUESTIONS].values.answer
  }),
  null
)(QuestionFormContainer);
