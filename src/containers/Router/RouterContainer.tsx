import * as React from "react";
import { Router } from "react-router-dom";

import history from "instances/history";

const RouterContainer = ({ children }: { children: any }) => (
  <Router history={history}>{children}</Router>
);

export default RouterContainer;
