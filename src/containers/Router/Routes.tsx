// @flow
import * as React from "react";
import { Switch, Route } from "react-router-dom";

import Page404 from "views/Page404";
import Questions from "views/Questions";
import Question from "views/Question";

type PropTypes = {};

class Routes extends React.Component<PropTypes> {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Questions} />
        <Route path="/question/:id" component={Question} />
        <Route component={Page404} />
      </Switch>
    );
  }
}

export default Routes;
