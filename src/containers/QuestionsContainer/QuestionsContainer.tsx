import React from "react";
import { connect } from "react-redux";
import { Grid, Container, Segment } from "semantic-ui-react";

import { listing } from "redux/modules/questions";
import { StoreType } from "redux/types";
import { Link } from "react-router-dom";
import Question from "components/Question";

type PropTypes = {
  fetch: Function;
  selectResults: Function;
};

class QuestionsContainer extends React.Component<PropTypes> {
  async componentDidMount() {
    await this.props.fetch({
      hashTableKey: ({ url }: { url: string }) => url.split("/")[2]
    });
  }

  render() {
    const results = this.props.selectResults();
    return (
      <Container>
        <Grid padded columns={4} stackable doubling>
          {Object.keys(results).map(key => {
            return (
              <Grid.Column key={key} width={4}>
                <Question {...results[key]} id={key} />
              </Grid.Column>
            );
          })}
        </Grid>
      </Container>
    );
  }
}

export default connect(
  (state: StoreType) => ({
    selectResults: listing.selectResults(state)
  }),
  {
    fetch: listing.fetchAllResultsAnd
  }
)(QuestionsContainer);
