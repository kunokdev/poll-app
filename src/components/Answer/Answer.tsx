import React from "react";
import { Segment, Grid, Popup } from "semantic-ui-react";

import styles from "./styles.module.scss";

interface PropTypes {
  id: string;
  selected: boolean;
  choice: any;
  voted: string | null;
  votes: number;
  percent: number;
  onClick: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

export default function Answer(props: PropTypes) {
  return (
    <Segment
      className={`${styles.element} ${(() => {
        if (props.voted && props.voted === props.id) return styles.voted;
        if (props.selected) return styles.selected;
        if (props.voted) return styles.disabled;
      })()}`}
      onClick={props.voted ? undefined : props.onClick}
    >
      <Grid>
        <Grid.Column width={8}>{props.choice}</Grid.Column>
        <Grid.Column width={4}>
          <Popup basic inverted trigger={<div>{props.votes}</div>}>
            <div>Vote count</div>
          </Popup>
        </Grid.Column>
        <Grid.Column width={4}>
          <Popup basic inverted trigger={<div>{`${props.percent}%`}</div>}>
            <div>Vote percent</div>
          </Popup>
        </Grid.Column>
      </Grid>
    </Segment>
  );
}
