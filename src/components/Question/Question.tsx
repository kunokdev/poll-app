import React from "react";
import { Segment } from "semantic-ui-react";
import moment from "moment";
import history from "instances/history";

import styles from "./styles.module.scss";

interface PropTypes {
  id: string;
  choices: [];
  question: string;
  published_at: string;
}

const Question = ({ id, choices, question, published_at: publishedAt }: PropTypes) => (
  <Segment className={styles.container} onClick={() => history.push(`/question/${id}`)}>
    <h1>{question}</h1>
    <p className={styles.date}>Published at {moment(publishedAt).format("MMM DD, YYYY")}</p>
    <p className={styles.total}>
      Total votes:{" "}
      {(() => {
        let buffer = 0;
        choices.forEach(({ votes }) => (buffer += votes));
        return buffer;
      })()}
    </p>
  </Segment>
);

export default Question;
