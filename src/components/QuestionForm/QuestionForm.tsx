import React from "react";
import { Field } from "redux-form";
import { Divider, Button } from "semantic-ui-react";

import s from "./styles.module.scss";
import { renderAnswers } from "utils/renderField";

export default class Form extends React.Component<any> {
  render() {
    return (
      <form className={s.form} onSubmit={this.props.handleSubmit}>
        <Field
          name="answer"
          voted={this.props.voted}
          answers={this.props.answers}
          selected={this.props.selectedAnswer}
          component={renderAnswers}
        />
        <Divider hidden />
        <Button color="blue" fluid size="large" disabled={Boolean(this.props.voted)} type="submit">
          {this.props.voted ? "You already voted" : "Submit"}
        </Button>
        {this.props.onCancel && (
          <button type="reset" onClick={this.props.onCancel}>
            <span role="img" aria-label="Cancel">
              ❌
            </span>{" "}
            Cancel
          </button>
        )}
      </form>
    );
  }
}
