import React from "react";
import { Icon, Container } from "semantic-ui-react";

import styles from "./styles.module.scss";

interface PropTypes {
  children: any;
}

const Header = () => (
  <header className={styles.header}>
    <Container>
      <h1>
        <Icon name="edit" />
        Poll app
      </h1>
    </Container>
  </header>
);

const BaseLayout = ({ children }: PropTypes) => (
  <div>
    <Header />
    <div>{children}</div>
  </div>
);

export default BaseLayout;
