export default function mapToArray(map: { [key: string]: any }): any[] {
  return Object.entries(map).map(([_, value]) => value);
}
