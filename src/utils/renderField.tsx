import React from "react";
import { CommonFieldInputProps } from "redux-form";

import Answers from "containers/Answers";

interface Input extends CommonFieldInputProps {
  onChange: Function;
}

export const renderAnswers = ({
  input,
  answers,
  selected,
  voted
}: {
  input: Input;
  answers: [];
  selected: string;
  voted: string | null;
}) => (
  <Answers
    {...input}
    answers={answers}
    voted={voted}
    selected={selected}
    onClick={(answer: any) => {
      const id = answer.url.split("/")[4];
      input.onChange(id);
    }}
  />
);
