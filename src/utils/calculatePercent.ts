export default function calculatePercentOutOfTotal(current: number, total: number) {
  return Math.round((current / total) * 100);
}
