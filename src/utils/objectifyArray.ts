import uuid from "uuid/v4";
import { resolveOrExpressionChain } from "./resolveExpressions";

type ReturnType = [{ [key: string]: string } | any, string[] | void];

const objectifyArray = (input: any, idProp?: string | number | Function | any): ReturnType => {
  if (Array.isArray(input)) {
    const output: any = {};
    const order: string[] = [];
    input.forEach(item => {
      const id = resolveOrExpressionChain(
        idProp && (typeof idProp === "function" ? idProp(item) : idProp),
        item.id,
        item._id,
        uuid()
      );
      order.push(id);
      output[id] = item;
    });
    return [output, order];
  }
  return [input, undefined];
};

export default objectifyArray;
