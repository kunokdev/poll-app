import { ExtendedActionType } from "redux/types";

type Behaviour = {
  initialState: {};
  reducer: Function;
};

export function behavingReducer(
  behaviours: Behaviour[],
  reducer?: Function,
  initialState?: {
    [key: string]: any;
  }
) {
  let composedInitialState = initialState || {};
  behaviours.reverse().forEach(behaviour => {
    if (!behaviour.initialState || typeof behaviour.initialState !== "object")
      throw new Error("Property `initialState` must be spreadable");
    composedInitialState = {
      ...composedInitialState,
      ...behaviour.initialState
    };
  });
  return (state = composedInitialState, action: ExtendedActionType) => {
    for (const behaviour of behaviours) {
      if (!behaviour.reducer) throw new Error("Property `reducer` is required on given object");
      const change = behaviour.reducer(state, action);
      if (change) return change;
    }
    if (!reducer) return state;
    return reducer({ ...composedInitialState, ...state }, action);
  };
}

// { reducer: Reducer, initialState: {}, ...public methods }
