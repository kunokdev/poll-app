export default function RSF(entity: string, type: string) {
  return [`${entity}/${type}_REQUEST`, `${entity}/${type}_SUCCESS`, `${entity}/${type}_FAILURE`];
}
