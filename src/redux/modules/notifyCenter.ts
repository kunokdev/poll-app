import { ExtendedActionType } from "redux/types";
import { Reducer } from "react";

export const CLEAR_NOTIFICATIONS = "notify/CLEAR_NOTIFICATIONS";
export const GENERIC_ERROR = "notify/GENERIC_ERROR";
export const GENERIC_SUCCESS = "notify/GENERIC_SUCCESS";

export interface INotifyCenterState {
  successMessage?: string | boolean | null;
  errorMessage?: string | boolean | null;
}

const initialState: INotifyCenterState = { successMessage: "", errorMessage: "" };

const reducer: Reducer<INotifyCenterState | undefined, ExtendedActionType> = (
  state: INotifyCenterState = initialState,
  action: ExtendedActionType
) => {
  if (action.type === CLEAR_NOTIFICATIONS)
    return { ...state, successMessage: "", errorMessage: "" };
  if (action.errorMessage)
    return { ...state, successMessage: "", errorMessage: action.errorMessage };
  if (action.successMessage)
    return {
      ...state,
      successMessage: action.successMessage,
      errorMessage: ""
    };
  return state;
};

export default reducer;

export function clearNotifications() {
  return function(dispatch: Function) {
    dispatch({ type: CLEAR_NOTIFICATIONS });
  };
}

export function showError(errorMessage: string) {
  return function(dispatch: Function) {
    dispatch({ type: GENERIC_ERROR, errorMessage });
  };
}

export function showSuccess(successMessage: string) {
  return function(dispatch: Function) {
    dispatch({ type: GENERIC_SUCCESS, successMessage });
  };
}
