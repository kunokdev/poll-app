import { behavingReducer } from "../common/redux-behave";
import Listing from "../behaviours/listing";
import Reading from "../behaviours/reading";
import Submission from "../behaviours/submission";

export const QUESTIONS = "questions";

export const listing = Listing(QUESTIONS);
export const reading = Reading(QUESTIONS, {
  resolveUrl: id => `/questions/${id}`
});
export const submission = Submission(QUESTIONS);

export const reducer = behavingReducer([listing, reading, submission]);
