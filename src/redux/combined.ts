import { combineReducers } from "redux";
import { reducer as toastr } from "react-redux-toastr";
import { reducer as form } from "redux-form";

import notifyCenter from "./modules/notifyCenter";
import { reducer as questions } from "./modules/questions";

export default combineReducers({
  notifyCenter,
  toastr,
  form,
  questions
});
