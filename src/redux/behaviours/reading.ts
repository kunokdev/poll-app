import RSF from "../common/RSF";
import { CALL_API } from "../middleware/api";
import { AxiosInstance } from "axios";
import { StoreType, ExtendedActionType } from "redux/types";

interface ReadingOptions {
  namespace?: string;
  resolveUrl: (id: string | number) => string;
}

export default function reading(
  module: string,
  { namespace = "reading", resolveUrl }: ReadingOptions
) {
  const prefix = `${module}/${namespace}`;
  const FETCH = "FETCH";
  const [FETCH_REQUEST, FETCH_SUCCESS, FETCH_FAILURE] = RSF(prefix, FETCH);
  const RESET = `${prefix}/RESET`;

  const initialState = {
    [`${namespace}Loading`]: false,
    [`${namespace}Success`]: false,
    [`${namespace}Error`]: false,
    [`${namespace}Payload`]: null
  };

  const fetch = (id: string | number, params?: any) => ({
    [CALL_API]: {
      types: RSF(prefix, FETCH),
      successMessage: false,
      ...params,
      promise: (httpClient: AxiosInstance) =>
        httpClient({
          method: "get",
          url: resolveUrl(id)
        })
    }
  });

  const reset = () => ({
    type: RESET
  });

  // Operations
  const fetchAnd = (id: string) => async (dispatch: Function) => {
    return await dispatch(fetch(id));
  };

  const resetAnd = () => async (dispatch: Function) => {
    return await dispatch(reset());
  };

  // Selectors
  const selectIsLoading = (state: StoreType) => () => state[module][`${namespace}Loading`];
  const selectIsSuccess = (state: StoreType) => () => state[module][`${namespace}Success`];
  const selectIsError = (state: StoreType) => () => state[module][`${namespace}Error`];
  const selectPayload = (state: StoreType) => () => state[module][`${namespace}Payload`];

  return {
    initialState,
    selectIsLoading,
    selectIsSuccess,
    selectIsError,
    selectPayload,
    fetchAnd,
    resetAnd,
    reducer: (state = initialState, action: ExtendedActionType) => {
      switch (action.type) {
        case FETCH_REQUEST:
          return {
            ...state,
            [`${namespace}Loading`]: true,
            [`${namespace}Success`]: false,
            [`${namespace}Error`]: false,
            [`${namespace}Payload`]: null
          };
        case FETCH_SUCCESS:
          return {
            ...state,
            [`${namespace}Loading`]: false,
            [`${namespace}Success`]: true,
            [`${namespace}Payload`]: action.payload
          };
        case FETCH_FAILURE:
          return {
            ...state,
            [`${namespace}Loading`]: false,
            [`${namespace}Error`]: true
          };
        case RESET:
          return initialState;
        default:
          return null;
      }
    }
  };
}
