import RSF from "../common/RSF";
import { CALL_API } from "../middleware/api";
import { AxiosInstance } from "axios";
import { StoreType, ExtendedActionType } from "redux/types";

export interface ListingOptions {
  namespace?: string;
  resolveUrl?: (module: string) => string;
}

export default function listing(
  module: string,
  { namespace = "listing", resolveUrl }: ListingOptions = {}
) {
  const prefix = `${module}/${namespace}`;
  const FETCH = "FETCH";
  const [FETCH_REQUEST, FETCH_SUCCESS, FETCH_FAILURE] = RSF(prefix, FETCH);

  const initialState = {
    [`${namespace}Loading`]: false,
    [`${namespace}Success`]: false,
    [`${namespace}Error`]: false,
    [`${namespace}Order`]: [],
    [`${namespace}Results`]: {}
  };

  const fetchAllResults = (params?: any) => {
    return {
      [CALL_API]: {
        types: RSF(prefix, FETCH),
        successMessage: false,
        ...params,
        promise: (httpClient: AxiosInstance) =>
          httpClient({
            method: "get",
            url: (resolveUrl && resolveUrl(module)) || `/${module}`
          })
      }
    };
  };

  // operations
  const fetchAllResultsAnd = (params?: any) => async (dispatch: Function) => {
    return await dispatch(fetchAllResults(params));
  };

  // selectors
  const selectIsLoading = (state: StoreType) => () => state[module][`${namespace}Loading`];
  const selectIsSuccess = (state: StoreType) => () => state[module][`${namespace}Success`];
  const selectIsError = (state: StoreType) => () => state[module][`${namespace}Error`];
  const selectOrder = (state: StoreType) => () => state[module][`${namespace}Order`];
  const selectResults = (state: StoreType) => () => state[module][`${namespace}Results`];

  return {
    initialState,
    fetchAllResults,
    fetchAllResultsAnd,
    selectIsLoading,
    selectIsSuccess,
    selectIsError,
    selectOrder,
    selectResults,
    reducer: (state = initialState, action: ExtendedActionType) => {
      switch (action.type) {
        case FETCH_REQUEST:
          return {
            ...state,
            [`${namespace}Loading`]: true,
            [`${namespace}Success`]: false,
            [`${namespace}Error`]: false
          };
        case FETCH_SUCCESS:
          return {
            ...state,
            [`${namespace}Loading`]: false,
            [`${namespace}Success`]: true,
            [`${namespace}Order`]: [...Object.keys(action.payload).map(id => id)],
            [`${namespace}Results`]: action.payload
          };
        case FETCH_FAILURE:
          return {
            ...state,
            [`${namespace}Loading`]: false,
            [`${namespace}Error`]: true
          };
        default:
          return null;
      }
    }
  };
}
