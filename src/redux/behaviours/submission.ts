import { CALL_API } from "redux/middleware/api";
import RSF from "redux/common/RSF";
import { AxiosInstance } from "axios";
import { StoreType, ExtendedActionType } from "redux/types";

export interface SubmissionOptions {
  namespace?: string;
}

export default function submission(
  module: string,
  { namespace = "submission" }: SubmissionOptions = {}
) {
  const prefix = `${module}/${namespace}`;
  const SUBMIT = "SUBMIT";
  const [SUBMIT_REQUEST, SUBMIT_SUCCESS, SUBMIT_FAILURE] = RSF(prefix, SUBMIT);

  const initialState = {
    [`${namespace}Loading`]: false,
    [`${namespace}Success`]: false,
    [`${namespace}Error`]: false,
    [`${namespace}Response`]: null
  };

  const submit = (url: string, data?: any, params?: {}) => ({
    [CALL_API]: {
      types: RSF(prefix, SUBMIT),
      successMessage: "Successfully voted",
      ...params,
      promise: (httpClient: AxiosInstance) =>
        httpClient({
          method: "post",
          url,
          data
        })
    }
  });

  // operations
  const submitAnd = (url: string, data?: any, params?: {}) => async (dispatch: Function) => {
    return await dispatch(submit(url, data, params));
  };

  // selectors
  const selectIsLoading = (state: StoreType) => () => state[module][`${namespace}Loading`];
  const selectIsSuccess = (state: StoreType) => () => state[module][`${namespace}Success`];
  const selectIsError = (state: StoreType) => () => state[module][`${namespace}Error`];
  const selectResponse = (state: StoreType) => () => state[module][`${namespace}Response`];

  return {
    initialState,
    submitAnd,
    selectIsLoading,
    selectIsSuccess,
    selectIsError,
    selectResponse,
    reducer: (state = initialState, action: ExtendedActionType) => {
      switch (action.type) {
        case SUBMIT_REQUEST:
          return {
            ...state,
            [`${namespace}Loading`]: true,
            [`${namespace}Success`]: false,
            [`${namespace}Error`]: false
          };
        case SUBMIT_SUCCESS:
          return {
            ...state,
            [`${namespace}Loading`]: false,
            [`${namespace}Success`]: true,
            [`${namespace}Response`]: action.payload
          };
        case SUBMIT_FAILURE:
          return {
            ...state,
            [`${namespace}Loading`]: false,
            [`${namespace}Error`]: true
          };
        default:
          return null;
      }
    }
  };
}
