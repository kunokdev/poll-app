import { instance as httpClient } from "instances/Axios";
import objectifyArray from "utils/objectifyArray";
import { Middleware } from "redux";
import { ExtendedActionType } from "redux/types";

export const CALL_API = "CALL_API";

function validateCallAPI(types: [string, string, string]) {
  if (!Array.isArray(types) || types.length !== 3)
    throw new Error("Expected an array of three action types.");
  if (!types.every(type => typeof type === "string"))
    throw new Error("Expected action types to be strings.");
  return true;
}

const apiMiddleware: Middleware = () => (next: Function) => async (
  action: ExtendedActionType
) => {
  const callAPI = action[CALL_API];

  if (typeof callAPI === "undefined") return next(action);

  const {
    types,
    promise,
    successMessage,
    errorMessage,
    payloadModifier,
    hashTableKey,
    payloadPropertyName,
    meta
  } = callAPI;

  validateCallAPI(types);

  const [requestType, successType, failureType] = types;
  next({ type: requestType, meta, ...action });

  try {
    const response = await promise(httpClient);

    let data =
      response &&
      response.data &&
      ((response &&
        response.data &&
        (payloadPropertyName && response.data[payloadPropertyName])) ||
        (response.data.payload ? response.data.payload : response.data));
    if (payloadModifier) data = payloadModifier(data);
    const [payload, order] = objectifyArray(data, hashTableKey);

    const nextAction = {
      type: successType,
      payload,
      meta: {
        ...meta,
        order
      },
      ...action
    };
    if (response && response.data && response.data.meta)
      nextAction.meta.responseMeta = response.data.meta;

    nextAction.successMessage =
      successMessage || (response && response.data && response.data.message);
    if (successMessage === false) nextAction.successMessage = false;

    return next(nextAction);
  } catch (err) {
    const nextAction = {
      type: failureType,
      payload: err,
      meta,
      ...action
    };

    const error =
      err.response && err.response.data && err.response.data.message;
    nextAction.errorMessage = error || errorMessage;

    return next(nextAction);
  }
};

export default apiMiddleware;
