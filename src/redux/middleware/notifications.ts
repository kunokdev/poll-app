import { toastr } from "react-redux-toastr";
import { ExtendedActionType } from "redux/types";

export default () => (next: Function) => async (action: ExtendedActionType) => {
  const { successMessage, errorMessage } = action;
  if (!successMessage && !errorMessage) {
    return next(action);
  }
  if (successMessage)
    toastr.light("Success!", String(successMessage), {
      icon: "success",
      status: "success"
    });
  if (errorMessage)
    toastr.light("Error!", String(errorMessage), {
      icon: "error",
      status: "error"
    });
  return next(action);
};
