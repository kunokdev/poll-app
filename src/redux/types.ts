import { FormReducer } from "redux-form";

export type ActionType = {
  type: string;
  payload: any;
};

export type ExtendedActionType = {
  type: string;
  payload: any;
  successMessage?: string | null | boolean;
  errorMessage?: string | null | boolean;
  CALL_API: ICallAPI;
  meta: {
    responseMeta?: any;
    order: string[];
    readOneId?: string;
  };
};

export type StoreType = {
  notifyCenter: any;
  toastr: {};
  form: {
    [key: string]: {
      values: {
        answer?: string;
      };
    };
  };
  [key: string]: any;
};

export interface ICallAPI {
  types: [string, string, string];
  promise: Function;
  successMessage?: string | null | boolean;
  errorMessage?: string | null | boolean;
  payloadModifier?: Function;
  hashTableKey?: string | number | Function;
  payloadPropertyName?: string;
  meta: any;
}
