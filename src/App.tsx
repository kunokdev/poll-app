import React from "react";
import { Provider } from "react-redux";
import Toastr from "react-redux-toastr";

import store from "redux/store";
import { RouterContainer, Routes } from "containers/Router";
import BaseLayout from "components/BaseLayout";

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <RouterContainer>
          <BaseLayout>
            <Routes />
            <Toastr transitionIn="fadeIn" transitionOut="fadeOut" position="top-center" />
          </BaseLayout>
        </RouterContainer>
      </Provider>
    );
  }
}

export default App;
