import React from "react";
import { Divider } from "semantic-ui-react";
import QuestionsContainer from "containers/QuestionsContainer";

export default class Questions extends React.Component {
  render() {
    return (
      <div className="fadeIn">
        <Divider hidden />
        <QuestionsContainer />
      </div>
    );
  }
}
