import React from "react";
import { Container, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";

import QuestionContainer from "containers/QuestionContainer";
import styles from "./styles.module.scss";

interface PropTypes {
  match: {
    params: {
      id: string;
    };
  };
}

export default class Question extends React.Component<PropTypes> {
  render() {
    const id = this.props.match && this.props.match.params && this.props.match.params.id;
    return (
      <Container className="fadeIn">
        <Link to="/questiions">
          <p className={styles.back}>
            <Icon name="chevron left" />
            Back to questions
          </p>
        </Link>
        <QuestionContainer id={id} />
        <Link to="/questiions">
          <p className={styles.back}>
            <Icon name="chevron left" />
            Back to questions
          </p>
        </Link>
      </Container>
    );
  }
}
