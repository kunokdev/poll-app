import * as React from "react";
import { Redirect } from "react-router-dom";

type PropTypes = {
  pushNotification: Function;
};

export default class Page404 extends React.Component<PropTypes> {
  componentDidMount() {
    // push notification
  }

  render() {
    return <Redirect to="/" />;
  }
}
