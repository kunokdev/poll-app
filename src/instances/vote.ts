interface IVote {
  setVote: (questionId: string, choiceId: string) => void;
  getVote: (questionId: string) => void;
}

class Vote implements IVote {
  setVote(questionId: string, choiceId: string) {
    const votes: { [key: string]: string } = JSON.parse(localStorage.getItem("votes") || "{}");
    votes[questionId] = choiceId;
    localStorage.setItem("votes", JSON.stringify(votes));
  }

  getVote(questionId: string) {
    const votes: { [key: string]: string } = JSON.parse(localStorage.getItem("votes") || "{}");
    return votes[questionId];
  }
}

export default new Vote();
