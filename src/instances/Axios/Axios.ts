import axios from "axios";
import { ExtendedWindow } from "types";

const { _env_ }: ExtendedWindow = window;
const baseURL = _env_ && _env_.API_URL;

export const instance = axios.create({
  baseURL: baseURL || "",
  timeout: 30000
});
